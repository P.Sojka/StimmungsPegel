package controllers;

import play.data.DynamicForm;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.addLocation;
import views.html.index;

import java.sql.*;


public class Application extends Controller {

    public static Result index() {

        return ok(index.render("Your new application is ready."));
    }


    public static Result addLocation() {

        return ok(addLocation.render());
    }

}
