package models;

/**
 * Created by Patrick-Sojka on 06.04.17.
 */
public class Location {

    double eintritt;        //Eintrittpreis
    int stimmung;           //stimmung wird gevotet
    int partypeople;        //Anzahl der leute vor ort
    String name;            //name der Location
    int typ;                //1=Club 2=Restaurant 3=kneipe
    double bierpreis;       //bierpreis
    String adresse;         //adresse der Location
//att veranstaltung

    public Location(double eintritt, int stimmung, int partypeople, String name, int typ, double bierpreis, String adresse) {
        this.eintritt = eintritt;
        this.stimmung = stimmung;
        this.partypeople = partypeople;
        this.name = name;
        this.typ = typ;
        this.bierpreis = bierpreis;
        this.adresse = adresse;
    }

    public Location() {
    }

    public double getEintritt() {
        return eintritt;
    }

    public void setEintritt(double eintritt) {
        this.eintritt = eintritt;
    }

    public int getStimmung() {
        return stimmung;
    }

    public void setStimmung(int stimmung) {
        this.stimmung = stimmung;
    }

    public int getPartypeople() {
        return partypeople;
    }

    public void setPartypeople(int partypeople) {
        this.partypeople = partypeople;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTyp() {
        return typ;
    }

    public void setTyp(int typ) {
        this.typ = typ;
    }

    public double getBierpreis() {
        return bierpreis;
    }

    public void setBierpreis(double bierpreis) {
        this.bierpreis = bierpreis;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }
}
